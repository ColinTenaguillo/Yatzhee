from random import randint

upper_section = {
    "ones": 1,
    "twos": 2,
    "threes": 3,
    "fours": 4,
    "fives": 5,
    "sixes": 6,
}

def roll_dices(dices, wich_dices):
    """Roll dices

    Args:
        dices (array): all_dices
        wich_dices (array): index of dices to roll
    """    
    for x in wich_dices:
        dices[x] = randint(1,6)

    return dices

def upper_combinations(dices, section):
    """calculate combinations board

    Args:
        dices (array): all dices
        type (string): string for combination type
    """    

    result = 0
    for dice in dices:

        if (dice == upper_section[section]):
            result += dice

    return result


def of_a_kind(dices, kind):
    """Calculate of given kind

    Args:
        dices (array): array of dice
        kind (int): given kind

    Returns:
        int: sum if kind else 0
    """    
    is_kind = False

    array = {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0
    }

    for x in dices:
        array[x] = array[x] + 1
        
    for x in array:
        if (array[x] >= kind):
            is_kind = True
    if (is_kind):
        return sum(dices)
    else:
        return 0


def three_of_a_kind(dices):
    return of_a_kind(dices, 3)

def four_of_a_kind(dices):
    return of_a_kind(dices, 4)
    
def yahtzee(dices):
    if (of_a_kind(dices, 5) != 0):
        return 50
    else:
        return 0

def fullhouse(dices):
    if (of_a_kind(dices, 3) != 0 and of_a_kind(dices, 2) != 0):
        return 25
    else:
        return 0

def straight(dices, possibilities):
    dices = sorted(dices)

    count = 0
    for x in possibilities:
        if count < len(x):
            count = len(set(dices) & set(x))

    return count

def small_straight(dices):
    possibilites = [
        [1,2,3,4],
        [2,3,4,5],
        [3,4,5,6],
    ]

    if straight(dices, possibilites) in [4,5] :
        return 30
    else:
        return 0

def large_straight(dices):
    possibilites = [
        [1,2,3,4,5],
        [2,3,4,5,6],
    ]

    if straight(dices, possibilites) == 5 :
        return 40
    else:
        return 0

def chance(dices):
    return sum(dices)

def show_scoreboard(dices):
    print(dices)

    print("Ones", upper_combinations(dices, "ones"))
    print("Twos", upper_combinations(dices, "twos"))
    print("Threes", upper_combinations(dices, "threes"))
    print("Fours", upper_combinations(dices, "fours"))
    print("Fives", upper_combinations(dices, "fives"))
    print("Sixes", upper_combinations(dices, "sixes"))
    print("Three of a kind", three_of_a_kind(dices))
    print("Four of a kind", four_of_a_kind(dices))
    print("Fullhouse", fullhouse(dices))
    print("Small straight", small_straight(dices))
    print("large straight", large_straight(dices))
    print("Yathzee", yahtzee(dices))

# General test

def test_valid_dices(dices):
    """Each dice should be between 1 and 6"""
    for x in dices:
        assert x <= 6
        assert x >= 1

def test_valid_number_of_dices(dices):
    """There should be only five dice"""
    assert len(dices) == 5

# Upper section combinations

def test_upper_combinations():
    dices = [1,1,1,6,6]

    assert upper_combinations(dices, "ones") == 3
    assert upper_combinations(dices, "sixes") == 12
    assert upper_combinations(dices, "threes") == 0


    dices = [5,3,5,4,2]
    assert upper_combinations(dices, "twos") == 2
    assert upper_combinations(dices, "threes") == 3
    assert upper_combinations(dices, "fours") == 4
    assert upper_combinations(dices, "fives") == 10
    assert upper_combinations(dices, "sixes") == 0 

# Lower section combinations

def test_three_of_a_kind():
    assert three_of_a_kind([1,1,1,6,6]) == 15
    assert three_of_a_kind([1,1,6,6,6]) == 20
    assert three_of_a_kind([1,5,1,6,6]) == 0
    assert three_of_a_kind([4,6,6,6,6]) == 28
    assert three_of_a_kind([2,1,2,6,2]) == 13


def test_four_of_a_kind():
    assert four_of_a_kind([1,1,1,6,6]) == 0
    assert four_of_a_kind([2,1,2,2,2]) == 9
    assert four_of_a_kind([6,6,6,6,4]) == 28
    assert four_of_a_kind([4,4,6,4,4]) == 22

def test_fullhouse():
    assert fullhouse([1,1,1,3,3]) == 25
    assert fullhouse([2,2,2,4,4]) == 25
    assert fullhouse([3,3,3,1,1]) == 25
    assert fullhouse([1,6,6,6,1]) == 25
    assert fullhouse([4,5,4,5,4]) == 25
    assert fullhouse([1,2,3,4,5]) == 0
    assert fullhouse([4,5,5,4,6]) == 0

def test_small_straight():
    assert small_straight([1,2,3,4,6]) == 30
    assert small_straight([2,3,4,5,1]) == 30
    assert small_straight([3,4,5,6,1]) == 30
    assert small_straight([2,3,4,5,6]) == 30
    assert small_straight([1,3,3,4,6]) == 0
    assert small_straight([1,2,3,5,6]) == 0

def test_large_straight():
    assert large_straight([1,2,3,4,5]) == 40
    assert large_straight([2,3,4,5,6]) == 40
    assert large_straight([3,4,5,6,1]) == 0
    assert large_straight([1,3,3,4,6]) == 0
    assert large_straight([1,2,3,5,6]) == 0

def test_chance():
    assert chance([1,2,3,4,5]) == 15
    assert chance([2,3,4,5,6]) == 20
    assert chance([3,4,5,6,1]) == 19
    assert chance([1,3,3,4,6]) == 17
    assert chance([1,2,3,5,6]) == 17

def test_yahtzee():
    assert yahtzee([1,1,1,1,1]) == 50
    assert yahtzee([2,2,2,2,2]) == 50
    assert yahtzee([3,3,3,3,3]) == 50
    assert yahtzee([4,4,4,4,4]) == 50
    assert yahtzee([5,5,5,5,5]) == 50
    assert yahtzee([6,6,6,6,6]) == 50
    assert yahtzee([6,6,6,4,6]) == 0
    assert yahtzee([6,2,2,2,6]) == 0

def test_all():
    dices = roll_dices([1,1,1,1,1], [0,1,2,3,4])
    test_valid_number_of_dices(dices)
    test_valid_dices(dices)

    test_upper_combinations()
    test_three_of_a_kind()
    test_four_of_a_kind()
    test_fullhouse()
    test_small_straight()
    test_large_straight()
    test_chance()
    test_yahtzee()    

# Main
if __name__ == "__main__":
    test_all()

    show_scoreboard(roll_dices([1,1,1,1,1], [0,1,2,3,4]))